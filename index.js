const getAllTodos = async () => {
  const result = await fetch("https://jsonplaceholder.typicode.com/todos");
  const json = await result.json();
  console.log(json);
};

const titles = async () => {
  const result = await fetch("https://jsonplaceholder.typicode.com/todos");
  const json = await result.json();

  const title = json.map((data) => data.title);
  console.log(title);
};

const todo1 = async () => {
  const result = await fetch("https://jsonplaceholder.typicode.com/todos/1");

  const json = await result.json();

  console.log(json);
  console.log(
    `The item "${json.title}" on the list has status of ${json.completed}`
  );
};

const postTodo = async () => {
  const result = await fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      title: "Created todo list",
      completed: false,
      userId: 1,
      id: 201,
    }),
  });

  const json = await result.json();
  console.log(json);
};

const updateWholeTodo = async () => {
  const result = await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      dateCompleted: "Pending",
      description: "To updated my todo list with a different data structure",
      title: "Updated todo list",
      completed: "Pending",
      userId: 1,
      id: 1,
    }),
  });

  const json = await result.json();
  console.log(json);
};

const updateSinglePropertyTodo = async () => {
  const result = await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      dateCompleted: "07/09/21",
      description: "To updated my todo list with a different data structure",
      title: "Updated todo list",
      completed: "Complete",
      userId: 1,
      id: 1,
    }),
  });

  const json = await result.json();
  console.log(json);
};

const deleteTodo = async () => {
  const result = await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE",
  });
};

getAllTodos();
titles();
todo1();
postTodo();
updateWholeTodo();
updateSinglePropertyTodo();
deleteTodo();
